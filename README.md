# xbrightness

X.org brightness control script for window managers.

## Requires

* `xrandr`
* `bash`
* GNU `awk`

**Don't forget to make the script executable!**

```
:~$ chmod +x /PATH/TO/SCRIPT/xbrightness.sh
```

## Version, help and usage

```
:~$ xbrightness.sh -h
XBrightness 1.0

Copyright (C) 2021 Blau Araujo <blau@debxp.org>
License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>.
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.

USAGE: xbrightness OPTION|VALUE

Without arguments, prints current brightness for all outputs.

OPTIONS:

    +   Increases current brightness by 0.1
    -   Decreases current brightness by 0.1
    r   Restores brightness to 1.1
    -h  Show this help

VALUE:

    Must be a float number in X.X format from 0.0 up to 5.5.

```


